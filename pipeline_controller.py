from clearml.automation.controller import PipelineController

# Creating the pipeline
pipe = PipelineController(
     name='pipeline test',
     project='pipeline',
     version='0.0.1',
     add_pipeline_tags=True
)

pipe.add_parameter(
    "training_data_folder",
    'data/mnist_png/training'
)
pipe.add_parameter(
    "testing_data_folder",
    'data/mnist_png/testing'
)

# set default queue
pipe.set_default_execution_queue("queue_name")

 # Adding the first stage to the pipeline, a clone of the base tasks will be created and used
pipe.add_step(name='stage_data',
                base_task_project='pipeline',
                base_task_name='pipeline step 1 dataset artifact',
                parameter_override={"General/training_path": "${pipeline.training_data_folder}",
                                    "General/test_path": "${pipeline.testing_data_folder}"})

# overriding the arguments of the the third stage(adding the second stage’s task_id) and adding it to the pipeline
pipe.add_step(name='stage_train',
                parents=['stage_data', ],
                base_task_project='pipeline',
                base_task_name='pipeline step 2 train model',
                parameter_override={
                    "General/dataset_name_training": "${stage_data.parameters.General/dataset_name_training}",
                    "General/dataset_name_test": "${stage_data.parameters.General/dataset_name_test}",
                    "General/dataset_project": "${stage_data.parameters.General/dataset_project}"}
                )

# Starting the pipeline
pipe.start_locally(run_pipeline_steps_locally=True)
# comment line above and uncomment line below to run scrips on queue test on which agents are listening
#pipe.start(queue='queue_name')

print('done')
