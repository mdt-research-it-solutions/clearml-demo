from clearml.automation import TriggerScheduler

# create the TriggerScheduler object (checking system state every minute)
trigger = TriggerScheduler(pooling_frequency_minutes=1.0)

# Add trigger on dataset creation
trigger.add_dataset_trigger(
    name='retrain on dataset',
    schedule_queue='test', 
    # You can also call a function that would get the latest experiment version
    # instead of hardcoding the training task ID
    schedule_task_id=' ceb1eaaa35a544ed95d9baf681f972ba',
    task_overrides={'General/3c975eefa4cc40ffad4d548cf2e78070': '${dataset.id}'},
    # What to trigger on
    trigger_project='pipeline',
)

# start the trigger daemon (locally/remotely)
# trigger.start()
trigger.start_remotely()
