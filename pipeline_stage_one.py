from clearml import Task, Dataset

task = Task.init(project_name="pipeline",
                task_name="pipeline step 1 dataset artifact",
                output_uri=True)

# only create the task, it will be executed remotely later
task.execute_remotely()

args = {
    'training_path': '',
    'test_path': '',
    'dataset_project': "pipeline",
    'dataset_name_training': "training_dataset",
    'dataset_name_test': "testing_dataset"
}

task.connect(args)

dataset_train = Dataset.create(
    dataset_name=args['dataset_name_training'],
    dataset_project=args['dataset_project']
)
dataset_test = Dataset.create(
    dataset_name=args['dataset_name_test'],
    dataset_project=args['dataset_project']
)

dataset_train.add_files(path=args['training_path'])
dataset_test.add_files(path=args['test_path'])

dataset_train.upload()
dataset_test.upload()

dataset_train.finalize()
dataset_test.finalize()

print('uploading datasets in the background')
print('Done')
